<?php
/**
 * @file
 * Contains doghouse_landing.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Implements hook_help().
 */
function doghouse_landing_helpers_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the boral_db_updates module.
    case 'help.page.doghouse_landing':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Content types, block types and paragraph types for building landing pages.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_preprocess_ds_entity_view().
 *
 * When IS using display suite.
 */
function doghouse_landing_preprocess_ds_entity_view(&$variables) {
  $entity = $variables['content']['#entity'];
  $variables['content']['#attributes']['class'] = isset($variables['content']['#attributes']['class']) ?
    $variables['content']['#attributes']['class'] : [];

  if ($entity->getEntityTypeId() == 'paragraph') {
    // Merge new classes in with existing classes.
    $variables['content']['#attributes']['class'] = array_merge(
      $variables['content']['#attributes']['class'],
      _doghouse_landing_get_classes($entity)
    );
  }
}

/**
 * Implements hook_entity_view_alter().
 *
 * When NOT using display suite.
 */
function doghouse_landing_entity_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
  $build['#attributes']['class'] = isset($build['#attributes']['class']) ?
    $build['#attributes']['class'] : [];

  if ($entity->getEntityTypeId() == 'paragraph') {
    // Merge new classes in with existing classes.
    $build['#attributes']['class'] = array_merge(
      $build['#attributes']['class'],
      _doghouse_landing_get_classes($entity)
    );
  }
}

/**
 * A helper that returns the classes based on the paragraph type.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *  The paragraph entity.
 *
 * @return array
 *   An array of classes to add to the wrapper.
 */
function _doghouse_landing_get_classes(EntityInterface $entity) {
  $classes = [];

  switch ($entity->bundle()) {
    case 'layout_flag':
      $position = $entity->get('field_image_position')->getString();
      $classes = ['layout-flag', 'layout-flag--' . $position];
      break;

    case 'layout_multi_column':
      $cols = $entity->get('field_column_count')->getString();
      $classes = ['layout-multi-column', 'layout-multi-column--' . $cols];
      break;

    case 'layout_text_on_image':
      $position = $entity->get('field_content_position')->getString();
      $classes = ['layout-text-on-image', 'layout-text-on-image--' . $position];
      break;

    case 'card':
      $classes = ['card'];
      break;
  }

  return $classes;
}
