# Doghouse Landing

This module provides the base config for landing pages and blocks. Similar to what page manager does
however it is geared towards content editors who want to build landing pages on the fly without any
development required. A common set of components (paragraph types) are installed during module install
and can be further customised from there.

Look in the [screenshots folder](http://stash.dhmedia.com.au/projects/DDMODULES/repos/doghouse-landing-drupal-8/browse/screenshots) 
to see what this module is trying to achieve.

## What it does do
 - Provides the initial structure for landing pages
 -- Content Type "Landing Page"
 -- Block Type "Landing Block"
 -- Paragraphs and fields required by the content type & block type.
 - Provides basic preprocessors to alter classes for the paragraph types
  
## What it does NOT do
 - Provide any styles
 - Provide any image stlyes or media view modes

## How to install
 - Ensure you have the doghouse packages repository in your `composer.json`
```
"repositories": [
        ...
        {
            "type": "composer",
            "url": "https://packages.doghouse.agency/"
        }
```
 - In terminal, run: `$ composer require doghouse/doghouse_landing`
 - Enable the module (EG `drush en doghouse_landing`) or add it as a dependency to your update module

## Once enabled
 - The module will import the config for landing page block, node, paragraph and fields
 - Style as required using the classes added to the paragraph bundles
 - Add media view modes to suit the images/media used
 - Edit manage display to use the correct media view modes
 
## CSS Classes that get applied
Classes get applied based on specific field values eg. flag position, text position, column count. See
_doghouse_landing_get_classes() in doghouse_landing.module. Base classes for these components also get
added.

## Contribute!
 - As this is mainly config that gets imported during install, if doing config updates, consider a module 
   like config_devel or completely uninstall/reinstall module after any changes to config.
